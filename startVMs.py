import sys

from lib import Shell
from lib.vm import vmConfigFileReader

if len(sys.argv) < 2:
    print("usage: startVMs.py <vmConfigFilePath>")
    exit(1)
vmConfigFilePath = sys.argv[1]
vms = vmConfigFileReader.readOnlyVMPath(vmConfigFilePath)

for vm in vms:
    print("starting vm: " + str(vm))
    try:
        Shell.execute("vmrun start \"" + str(vm.vmPath) + "\"", logCommand=False)
    except Exception as e:
        print("error starting vm, skipping..." + " caused by: " + str(e))




