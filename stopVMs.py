from lib import Shell
from lib.vm import vmConfigFileReader
import sys


if len(sys.argv) < 2:
    print("usage: startVMs.py <vmConfigFilePath>")
    exit(1)
vmConfigFilePath = sys.argv[1]
vms = vmConfigFileReader.readOnlyVMPath(vmConfigFilePath)

for vm in vms:
    print("stopping vm: " + str(vm))
    try:
        Shell.execute("vmrun -T ws stop \"" + str(vm.vmPath) + "\" soft")
    except Exception as e:
        print("error stopping vm, skipping..." + " caused by: " + str(e))
