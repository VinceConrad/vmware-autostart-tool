from lib import configFileReader
from lib.vm.vm import Vm

delimiter = " <D> "

def readVMConfigFile(fname):
    configLines = configFileReader.readConfigFile(fname)
    vms = list()
    for line in configLines:
        try:
            vms.append(Vm(line, delimiter))
        except SyntaxError as e:
            print(str(e) + " , skipping this vm")
    return vms

def readOnlyVMPath(fname):
    configLines = configFileReader.readConfigFile(fname)
    vms = list()
    for line in configLines:
        vms.append(Vm(line, delimiter, onlyPath=True))
    return vms
