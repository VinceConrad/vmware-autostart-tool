#reads all lines in file except comments
def readConfigFile(fname):
    with open(fname) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]

    entries = list()
    for line in content:
        if len(line) > 0:
            if line[0] != "#":
                entries.append(line)

    return entries
